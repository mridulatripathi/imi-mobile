'use strict';
im.directive('graph', function () {
    return {
        restrict: 'E',
        templateUrl: "app/directives/graph.html",
        controller: function ($http) {
            var paper = Raphael("score", 1366, 768),
                raphaelUtils = new RaphaelUtils(paper),
                center = [800, 700],
                axesValuesMultiplier = 80,
                awayBoundary = {a: [800, 300], b: center},
                homeBoundary = {a: center, b: [400, 700]},
                i,
                game,
                node = {};
            /*creates axes from points*/
            function createAxes() {
                raphaelUtils.stLine(homeBoundary.a[0], homeBoundary.a[1], homeBoundary.b[0], homeBoundary.b[1]);
                raphaelUtils.stLine(awayBoundary.a[0], awayBoundary.a[1], awayBoundary.b[0], awayBoundary.b[1]);
                raphaelUtils.stLine(center[0], awayBoundary.a[1], homeBoundary.b[0], awayBoundary.a[1], '#D3D3D3');
                raphaelUtils.stLine(homeBoundary.b[0], awayBoundary.a[1], homeBoundary.b[0], center[1], '#D3D3D3');
            }

            /*creates text nodes to denote axes node values*/
            function createAxesIntervals() {
                for (i = 0; i <= 5; i++) {
                    paper.text(center[0] - (axesValuesMultiplier * i), center[1] + 60, i);
                    i && paper.text(center[0] + 60, center[1] - (i * axesValuesMultiplier), i);
                }
                paper.text(center[0] - 200, center[1] + 60, 'HOME');
                paper.text(center[0] + 60, center[1] - 200, 'AWAY');
            }

            /*joins probability circles to axes*/
            function joinCenterAndAxes() {
                raphaelUtils.stLine(center[0] - (axesValuesMultiplier * game.home), center[1], node.x, node.y, '#D3D3D3', '1');
                raphaelUtils.stLine(center[0], center[1] - (game.away * axesValuesMultiplier), node.x, node.y, '#D3D3D3', '1');
                raphaelUtils.stLine(center[0] - (axesValuesMultiplier * game.home), awayBoundary.a[1], node.x, node.y, '#D3D3D3', '1');
                raphaelUtils.stLine(homeBoundary.b[0], center[1] - (game.away * axesValuesMultiplier), node.x, node.y, '#D3D3D3', '1');
            }

            /*-----axes----*/
            createAxes();
            createAxesIntervals();
            /*-----axes----*/

            $http.get("app/data/data.json").then(function (data) {
                let point = data.data, radius, fill, key;

                for (i = 0; i < point.length; i++) {

                    /*radius based on percent*/
                    radius = point[i].percent * 4;

                    game = point[i];

                    node.x = center[0] - (axesValuesMultiplier * game.home);
                    node.y = center[1] - (axesValuesMultiplier * game.away);

                    if (game.home > game.away) {
                        /*win*/
                        fill = '#7fd4fc';
                        key = 'home';
                    } else {
                        if (game.home === game.away) {
                            /*tie*/
                            fill = '#edf55b';

                            /*joining the center and axes*/
                            raphaelUtils.stLine(center[0], center[1], node.x, node.y, '#D3D3D3', '1');
                            raphaelUtils.stLine(homeBoundary.b[0], awayBoundary.a[1], node.x, node.y, '#D3D3D3', '1');

                        } else {
                            /*loss*/
                            fill = '#fdc055';
                            key = 'away';
                        }
                    }
                    if (game[key] < 6) {
                        joinCenterAndAxes();
                    }
                    var toolTip = "Home: " + game.home + ", \nAway: " + game.away + ", \nPercent: " + game.percent + ", \nSplit:" + game.splitUp;
                    raphaelUtils.bubble(node.x, node.y, radius, fill, toolTip);
                }
            });

        }
    };
});