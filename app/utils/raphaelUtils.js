function RaphaelUtils(instance) {
    this.instance = instance;
    this.bubble = function (cx, cy, r, fill, toolTip) {
        if (isNaN(r)) {
            return;
        }
        this.instance.circle(cx, cy, r).attr("fill", fill).attr('stroke-width', 0).attr('title', toolTip);
    };
    this.stLine = function (fromX, fromY, toX, toY, fill) {
        fill = fill || "#000";
        return this.instance.path(["M", fromX, fromY, "L", toX, toY]).attr("stroke-width", 1).attr("stroke", fill);
    };
}