# Bubble Graph

# Assumptions
1. The axes coordinates are arbitrary values.
2. Single unit is also arbitrarily created.


### Libraries used
1. Angular
2. Raphael

### Installation
Clone the repo
```sh
git clone https://mridulatripathi@bitbucket.org/mridulatripathi/imi-mobile.git
npm install
bower install

node app.js
```

Access the app at http://localhost:3000/